# Generating a bootable image for the Beaglebone Black using Yocto
This page presents a step-by-step guid on how to setup a building environment to generate a bootable linux image with rootfs for the Beaglebone black hardware

## Host environment setup
### Host machine details
- VMware image running Debian 11 bulleyes
- 100 GB allocated disk space (everything is rebuilt from source with yocto so it requires at least 60GB of free space on top of the rest of the system)
- 8GB Ram

### Packages needed beforhand
As recommenced in official documentation
[build-host-packages](https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html#build-host-packages) the following packages are needed for the yocto build and tools:

`sudo apt install gawk wget git diffstat unzip texinfo gcc build-essential chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm python3-subunit mesa-common-dev zstd liblz4-tool`

### Set expected locale
If the Linux host is configured with a different locale than en_US.UTF8, Yocto will likely complain about it before the build and it could create build issues later. There migth be a clean way arround this but in doubt, you can always add the required expected locale on the host system as metnioned in the documentation [here](https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues)

`sudo apt-get install locales`

Pick the en_US.UTF8 from the list

`sudo dpkg-reconfigure locales`

`sudo locale-gen en_US.UTF-8`

`sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8`

`export LANG=en_US.UTF-8`

### Toolchain
Yocto's approach to build the embedded linux distribution is to rebuild everything from scratch on the first run. The linux kernel, booloader, tools, libraries, applications...
To do so a compatible toolchain for the beaglebone black is needed. The board is built around the TI AM335x application processor which is an ARM based CPU. An ARM cross compiler is thus needed

ARM website provides prebuilt toolchains for different host/target environments. In our case we want a linux 64bits host toolchain that targets an ARM with linux as the target OS

[ARM x86_64-arm_none_linux_gnueabi v 9.2-2019.12](https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz?revision=fed31ee5-2ed7-40c8-9e0e-474299a3c4ac&hash=EA6FEBF1AF27143C49DF9FEE148AE3CC9EF4C064)

- Create a folder on the host where the toolchain will be installed (/usr/local/toolchains/arm here)

`sudo mkdir -p /usr/local/toolchains/arm`

`sudo chown -R youruser:yoursuser /usr/local/toolchains`

- Unpack and copy the toolchain

`tar -xvf gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz -C /usr/local/toolchains/arm`

- Yocto uses the `TOOLCHAIN_PATH` environment variable (if set) to locate the toolchain to be used for the build

`export TOOLCHAIN_PATH=/usr/local/toolchains/arm/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf`

## Yocto build environment setup

### OE Layer Setup script

There exists a couple of ways to setup the build environment for the upcoming Yocto build. I prefer to use the TI arago project oe setup script that will do most of the job for us

- Clone this repository somewhere on the host (if you have a host environment with multiple partitions, make sure the clone is located in the partition where the 60GB free space (or more) is located)

`git clone git@gitlab.com:achanu/yoctobeagleboneblack.git yocto_layersetup`

The two setup scripts in the root of this repo needed for setting up the environment come from the TI arago git repo (arago is TI linux distribution project for their supported hardware (like the AM335x) ):

- [oe-layertool-setup.sh](http://arago-project.org/git/projects/oe-layersetup.git?p=projects/oe-layersetup.git;a=blob;f=oe-layertool-setup.sh;hb=HEAD)
- [git-retry.sh](http://arago-project.org/git/projects/oe-layersetup.git?p=projects/oe-layersetup.git;a=blob;f=git_retry.sh;hb=HEAD)	

The file/folders will look like:

```bash
yocto_layersetup/
├── configs
│   └── bbb-config.txt
├── git_retry.sh
├── oe-layertool-setup.sh
└── sample-files
    ├── bblayers-dunfell-browser.conf
    └── local.conf.bbb
```

### Layer configuration files
#### bbb-config.txt
The oe-layertool-setup script will be used with the bbb-config.txt file that specifies which yocto layers repositiories and branch/rev are used for the build

This file specifies the following layer repos to be used:

   - bitbake

   - poky

   - openembedded

   - meta-ti

   - meta-arm

It also specifies the template layer configuration file and local configuration file that are to be used to genera the actual configuration files when creating the build environment:
- `OECORELAYERCONF=./sample-files/bblayers-dunfell-browser.conf`
- `OECORELOCALCONF=./sample-files/local.conf.bbb`

The Yocto project periodically releases new versions of its different components. Some of these releases are targetted for long term support (LTS). The current LTS release at the time of this writting is Dunfell. As such, all most of the yocto and third party layers repositories are cloned to use the dunfell branch which usually insure that they have been tested with this yocto release. However the layer configuration file bbb-config.txt clones the latest from HEAD inside the dunfell branch so future commits in these repo could cause issue. To prevent this, a specific commit rev can be provided instead of HEAD. 

For reference, here at the banch/revs pointing to HEAD of their respective branched at the time of writting this which were used to generate a bootable iamge:
| repo | branch | rev |
| ---      | ---      | ---      |
| bitbake   | 1.46   | be6ecc160ac4a8d9715257b9b955363cecc081ea   |
| poky   | dunfell   | bba323389749ec3e306509f8fb12649f031be152   |
| openembedded   | dunfell   | ab9fca485e13f6f2f9761e1d2810f87c2e4f060a   |
| meta-ti   | dunfell   | 40728f2d24ad1625e98fc4294d9dfa872031710f   |
| meta-arm   | dunfell   | c4f04f3fb66f8f4365b08b553af8206372e90a63   |


#### local.conf.bbb
This template file sets many Yocto specific default configuration variables like recipe download location, build location, cache usage ... It will be used to create the local.conf configuration file in the build folder that will be used during the build. Changing a value there after the environment has been created will require re running the setup script so that it takes effect in the updated build/conf folder

This file also specifies the default machine (target) where the image will be built for. Yocto provides by default, in its poky reference distribution, some actual embedded linux hardware targets that can be used without having to define a custom machine. Fortunately the beablebone is one of them so there is no need to create a custom machine and a BSP layer. The BSP layer is also provided by Yocto in the poky meta-yocto-bsp layer.

### Create the build environment
From the root of the yocto-layersetup folder, use the script with the layer configuration file to create the build envrionment: 

`./oe-layertool-setup.sh -f configs/bbb-config.txt`

This will:
- Create a sources folder that will contain the cloned repositories specified in the layer configuration file
- Create a build/conf folder that will contain the generated local.conf configuration file from the local.conf.bbb template file

Enter the build folder and source the environment file to load the environment variables and tools for the build (this is only valid in your current shell):

`cd build && . conf/setenv`

Notice the space between the `.` and `conf/setenv` !

## Linux kernel and u-boot versions
The version of the different components that will be built depend on the content of their recipes inside the different layers. Here, for Yocto dunfell for example the base linux recipes are found in the meta-ti layer at meta-ti/recipes-kernel/linux/. However, any recipe from a given layer can be overriden in an equivalent recipe in another layer to customize/modify it for other purposes (that's one the biggest selling point of yocto ... well it's free but still ). The presence of a recipe inside a layer does not mean it is used either. Depending on which image is built with what distro (poky, custom...) for what machine some recipes might be included or not in the final distribution. Here the linux-ti-staging-5.4 recipe inside the meta-layer is the one being used for the image we build. Similarly the U-boot recipe is located at meta-ti/recipes-bsp/u-boot/u-boot.inc wgich fetches the source from the TI git repo

## Image build
The environment is now setup ready to start the build
All of the tasks required to fetch the sources, configure them, build them and package them is done by the bitbake tool. Bitbake is a task scheduler for Yocto that looks at all the recipes (.bb files inside all the layers) and sequences them in the proper order to satisfy dependencies between them.

Yocto can be used to generate distribution image or SDKs that can be redistributed for development. Multiple default images exist that contain different set of packages ranging from limited console only image to full fledge graphical environment image. Here, we will build the console only base image known as core-base-image. More information about which images does what is explained in the [official documentation] (https://docs.yoctoproject.org/ref-manual/images.html#images)

Start the build from the yocto-layersetup/build folder:

`bitbake core-image-base`

This will take quite some time to finnish, specially the first time, as bitbake will parse all the recipes required for the image and will fetch the sources and build them before packaging them. Sometimes, some fetch operation will faill due to slow internet speed or too many fetches at the same time. If errors happen during the build, try to issue the bitbake command above again. If the same recipe fails again, then it might be related to something else...

If some errors occur quickly, make sure that :
- The `TOOLCHAIN_PATH` environment variable has been set to point to the location of the toolchain on the host
- The bitbake command is executed from the build folder and from the same shell where the environment was sourced before using `. conf/setenv`

If some errors occur after some time for certain recipe :
- Retry the bitbake image build command as sometimes the problem (even building related) can be due to a slow fetch of a recipe
- If the error is at the do_compile step, the problem might be related to an imcompatibility between the toolchain version and the recipe sources in which case you can:
-   try a more recent/older toolchain version (but the whole build will need to be restarted)
-   use another version of the failing recipe (you will need to create a custom meta layer folder that overrides the recipe version and add these custom layer to the layer configuration file and regenerate the build environment, howto can be found online)   

When build finnishes sucessfully, a `core-image-base-beaglebone-yocto.wic` image file should be available under `build/tmp/deploy/images/beaglebone-yocto`


## Image output and SD card creation
The .wic image contains the second stage bootloader, u-boot, kernel and rootfs with  installed packages 

In order to boot the image on the beaglebone black, we need an SD card that is mounted at a know location on the host (ex: /dev/sdc)

To know which device to use, run `lsblk` before and after having inserted the SD card in the USB adapter connected to the host. The new device displayed is the one used to mount the SD card, keep note of it 

### Booting from the SD card on the beaglebone black
Here are some things to know before we describe how to write the image on the SD card:

- By default the beaglebone will boot the OS isntalled in the internal EMCC (Flash) of the device, even if an SD card is inserted in the slot
- To force the device to boot from the SD card, power off the beaglebone and with the SD card inserted in the slot, press and hold the small button located roughly at the same place where the SD card slot is but on the other side of the board (the button is thus near the expansion header on top of the sd card slot)
- Power on the beagle bone while keeping the button pressed until you see serial debug output on the screen (see [Serial console section](#Serial console and logging) for more info)

### Yocto Wic tool (did not work for me)
The Yocto documentation mentions that the .wic image file needs to be used in conjunction with a .wks definition file with the wic tool to create and partition a resulting image that can then be written on a SD card or usb stick using dd https://www.yoctoproject.org/docs/2.4.2/dev-manual/dev-manual.html#creating-partitioned-images-using-wic 

-`wic create beaglebone-yocto -e core-base-image`

-`dd if=/path/to/resultingimage of=/dev/sdc` (replace /dev/sdc with the device name of your sd card as shown above)

I might have done something wrong there but the resulting image is missing the rootfs so when booting the beaglebone with it, the kernel can't jump into the rootfs and crashes. I did not spend too much on this as I knew of another way of doing this...

### Direct wic file write
Using the wic, we can directly write it to the SD card using dd since it contains everything we need (using a reasonable block size so it does not take forever)
-`dd if=build/tmp/deploy/images/beaglebone-yocto/core-image-base-beaglebone-yocto.wic of=/dev/sdc bs=1M` (replace /dev/sdc with the device name of your sd card as shown above)

With this SD image, I had no problem booting the beaglebone with it

### Serial console and logging
Plug a serial to usb cable between the serial debug header and the host and fire any serial com tool (putty, minicom...). The typical device that is populated to map the serial interface is /dev/ttyUSB0 but this could be different on another host. Inspect /dev or dmesg or even lsusb to know which device to use

Use the typical serial configuration 8N1 at 115200

By default with this image, the only user is root and there's no password

## References
[Yocto official documentation portal](http://docs.yoctoproject.org/)

[TI SDK Yocto guide](https://software-dl.ti.com/processor-sdk-linux/esd/docs/latest/linux/Overview_Building_the_SDK.html)

[TI arago SDK setup guide](http://arago-project.org/wiki/index.php/Setting_Up_Build_Environment)

[TI arago git repo](http://arago-project.org/git/projects/oe-layersetup.git)

[Beaglebone black Yocto guide](https://beagleboard.org/p/30847/yocto-on-beaglebone-black-9ae649)